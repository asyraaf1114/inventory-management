<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Sale;
use App\Sale_Product;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Cart::content();
        return view ('carts.index', compact('carts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->id;
        $total = 0;

        $carts = Cart::content();
        foreach ($carts as $cart){
            $total += $cart->subtotal;
        }

        $sale= new Sale;
        $sale->user_id = $user_id;
        $sale->total_price = $total;
        $sale->date = Carbon::now();
        $sale->save();

        foreach ($carts as $cart) {
            $product = Product::findOrFail($cart->id);
            $product->quantity -= $cart->qty;
            $product->save();

            $sale_product = new Sale_Product;
            $sale_product->sale_id = $sale->id;
            $sale_product->product_id = $cart->id;
            $sale_product->quantity = $cart->qty;
            $sale_product->subtotal = $cart->subtotal;
            $sale_product->save();
        }

        Cart::destroy();
        
        return redirect()->route('carts.index')->with('statusSuccess', 'Successfully Paid');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::where('barcode', '=', $request['barcode'])->first();
        if ($product){
            Cart::add(
            [
                'id' => $product->id,
                'name' =>$product->name,
                'qty' => 1,
                'price' =>$product->price
            ]);
            return redirect()->route('carts.index')->with('statusSuccess', 'Item Added');
        } else {
            return redirect()->route('carts.index')->with('statusFail', 'Item Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($rowId)
    {
        $item = Cart::get($rowId);
        Cart::update($rowId, ['qty'=>$item->qty - 1]);
        return redirect()->route('carts.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($rowId)
    {
        $item = Cart::get($rowId);
        Cart::update($rowId, ['qty'=>0]);
        return redirect()->route('carts.index');
    }

    public function add($id, $quantity) 
    {
        $item = Cart::get($id);
        Cart::update($id, ['qty'=>$quantity]);
        return redirect()->route('carts.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::destroy();
        return redirect()->route('carts.index')->with('statusSuccess', 'Cart Successfully Cleared');
    }
}
