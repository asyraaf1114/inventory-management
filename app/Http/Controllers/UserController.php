<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => 'required|string|min:3',
            'email'     => 'required|string|email|max:255|unique:users',
            'role'      => 'required',
            'password'  => 'required|string|min:6',
            'confirm_password' => 'required|same:password',
        ]);

        $request['password'] = bcrypt($request->get('password'));
        User::create($request->all());

        return redirect()->route('users.index')->with('status','User successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if($request->input('password') || $request->input('confirm_password')) {
            $rules = [
                'name'              => 'required|string|min:3',
                'email'             => 'required|string|email|max:255|unique:users,email,'.$user->id,
                'password'          => 'min:6',
                'confirm_password'  => 'same:password',
            ];
            $this->validate($request, $rules);
            $request['password'] = bcrypt($request['password']);
        } else {
            $rules = [
                'name'      => 'required|string|min:3',
                'email'     => 'required|string|email|max:255|unique:users,email,'.$user->id,
            ]; 
            $request['password'] = $user->password;
            $this->validate($request, $rules);
        }      
        
        $user->update($request->all());

        return redirect()->route('users.index', ['id' => $id])->with('status', 'Data Successfully Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index')->with('status', 'User successfully deleted');
    }
}
