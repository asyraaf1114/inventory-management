<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('profile.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if($request->input('password') || $request->input('confirm_password')) {
            $rules = [
                'name'              => 'required|string|min:3',
                'email'             => 'required|string|email|max:255|unique:users,email,'.$user->id,
                'password'          => 'min:6',
                'confirm_password'  => 'same:password',
            ];
            $this->validate($request, $rules);
            $request['password'] = bcrypt($request['password']);
        } else {
            $rules = [
                'name'      => 'required|string|min:3',
                'email'     => 'required|string|email|max:255|unique:users,email,'.$user->id,
            ]; 
            $request['password'] = $user->password;
            $this->validate($request, $rules);
        }      
        
        $user->update($request->all());

        return redirect()->route('profile.index', ['id' => $id])->with('status', 'Profile Successfully Edited');
    }

 
}
