<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $category = Category::orderBy('name','ASC')
            ->get()
            ->pluck('name','id');

        return view('products.index', compact('products', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => 'required|string|min:3|unique:products',
            'category_id'  => 'required',
            'price' => 'required',
            'quantity'     => 'required|integer',
            'min_quantity'     => 'required|integer',
            'barcode' => 'required|string|unique:products'
        ]);

        $input = $request->all();
        $input['image'] = null;

        if ($request->hasFile('image')){
            $input['image'] = '/upload/products/'.str_slug($input['name'], '-').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('/upload/products/'), $input['image']);
        }

        Product::create($input);

        return redirect()->route('products.index')->with('status','Product successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $category = Category::orderBy('name','ASC')
            ->get()
            ->pluck('name','id');

        return view('products.show', compact('product', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::all();
        return view('products.edit',compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $this->validate($request,[
            'name'      => 'required|string|min:3|unique:products,name,'.$product->id,
            'category_id'  => 'required',
            'price' => 'required',
            'quantity'     => 'required|integer',
            'min_quantity'     => 'required|integer',
            'barcode' => 'required|string|unique:products,barcode,'.$product->id,
        ]);

        $input = $request->all();
        $input['image'] = $product->image;

        if ($request->hasFile('image')){
            $input['image'] = '/upload/products/'.str_slug($input['name'], '-').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('/upload/products/'), $input['image']);
        }

        $product->update($input);

        return redirect()->route('products.index')->with('status','Product successfully created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if (!$product->image == NULL){
            unlink(public_path($product->image));
        }
        $product->delete();
        return redirect()->route('products.index')->with('status', 'Product successfully deleted');
    }
}
