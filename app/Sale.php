<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['user_id','total_price','date'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
