<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale_Product extends Model
{
    protected $fillable = ['sale_id','product_id','quantity','subtotal'];
    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
