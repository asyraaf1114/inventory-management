<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['category_id','name','price','quantity','min_quantity','barcode','image'];

    protected $hidden = ['created_at','updated_at'];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
}
