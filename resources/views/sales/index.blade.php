@extends('layouts.master')

@section('top')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
    <h3 style="margin-top:0px">Sales Records</h3>
    @if(session('status'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{ session('status') }}
            </div>
        </div>
    @endif

    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">List of Sales</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="sale-table" class="table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Cashier</th>
                        <th>Date</th>
                        <th>Products</th>
                        <th>Total Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sales as $sale)
                        <tr>
                            <td>{{ $sale->id }}</td>
                            <td>{{ $sale->user->name }}</td>
                            <td>{{ $sale->date}}</td>
                            <td>
                                @foreach ($sales_products as $sale_product)
                                    @if($sale_product->sale->id == $sale->id)
                                        {{ $sale_product->product->name }} x {{$sale_product->quantity}}<br>
                                    @endif
                                @endforeach
                            </td>
                            <td>RM {{ $sale->total_price }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
    <!-- DataTables -->
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>

    <script>
        $(function () {
            $('#sale-table').DataTable()
        })

    </script>
@endsection