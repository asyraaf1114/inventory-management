@extends('layouts.master')

@section('top')
@endsection

@section('content')
    <h3 style="margin-top:0px">Sales</h3>
    <div class="box-header">
        <a href="{{ route('sales.index') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> BACK</a>
    </div>
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">View Sale</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <h5>Seller : <b>{{ $sale->user->name}} </b></h5>
            <h5>Date : <b>{{ $sale->date}} </b></h5>
            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Sub Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->product->name }}</td>
                            <td>{{ $product->quantity }}</td>
                            <td>{{ $product->subtotal }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-footer">
                <h4 class="pull-right"><b>Total : {{ $sale->total_price }}</b></h4>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
@endsection
