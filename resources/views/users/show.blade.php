@extends('layouts.master')

@section('top')
@endsection

@section('content')
    <h3 style="margin-top:0px">Users</h3>
    <div class="box-header">
        <a href="{{ route('users.index') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> BACK</a>
    </div>
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">View User</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped datatable">
                <tr>
                    <th>ID</th>
                    <td>{{ $user->id }}</td>
                </tr>

                <tr>
                    <th>Name</th>
                    <td>{{ $user->name }}</td>
                </tr>

                <tr>
                    <th>Email</th>
                    <td>{{ $user->email }}</td>
                </tr>

                <tr>
                    <th>Role</th>
                    <td>{{ $user->role }}</td>
                </tr>

                <tr>
                    <th>Previous Sign In</th>
                    <td>{{ $user->sign_in_at }}</td>
                </tr>

                <tr>
                    <th>Previous Sign Out</th>
                    <td>{{ $user->sign_out_at }}</td>
                </tr>

                <tr>
                    <th>Created at</th>
                    <td>{{ $user->created_at }}</td>
                </tr>

                <tr>
                    <th>Updated at</th>
                    <td>{{ $user->updated_at }}</td>
                </tr>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
@endsection
