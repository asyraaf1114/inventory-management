@extends('layouts.master')

@section('top')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <h3 style="margin-top:0px">Users</h3>
    <!-- general form elements -->
    <div class="box-header">
        <a href="{{ route('users.index') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> BACK</a>
    </div>
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Create User</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="POST" action="{{ route('users.store')  }}" autocomplete="off">
            @csrf

            <div class="box-body">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" value="{{ old('name') }}" id="name" class="form-control" placeholder="Enter name" required>
                    @if ($errors->has('name'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('name') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label>Email Address</label>
                    <input type="email" name="email"  value="{{ old('email')  }}" id="email" class="form-control" placeholder="Enter email" required>
                    @if ($errors->has('email'))
                        <br>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('email') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label>Role</label>
                    <select class="form-control select_role" value="{{ old('role')  }}" name="role" id="role" required>
                        <option value="">-- Select Role --</option>
                        <option value="admin" {{ (Input::old("role") == "admin" ? "selected":"") }}>Admin</option>
                        <option value="user" {{ (Input::old("role") == "user" ? "selected":"") }}>User</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    @if ($errors->has('password'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('password') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password">
                    @if ($errors->has('confirm_password'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('confirm_password') }}
                        </div>
                    @endif
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> CREATE</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
@endsection

@section('bot')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(".select_role").select2();
</script>
@endsection
