@extends('layouts.master')

@section('top')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
    <h3 style="margin-top:0px">Users</h3>
    @if(session('status'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{ session('status') }}
            </div>
        </div>
    @endif

    <div class="box-header">
        <a href="{{ route('users.create') }}" class="btn btn-primary" ><i class="glyphicon glyphicon-plus"></i> Add User</a>
    </div>

    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">List of Users</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="user-table" class="table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Last Sign In</th>
                        <th>Last Sign Out</th>
                        @if(Auth::user()->isSuperOrAdmin())
                            <th>Action</th>
                            <th>Delete</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td class="center"><div class="badge bg-green">{{ $user->role }}</div></td>
                            <td>{{ $user->sign_in_at }}</td>
                            <td>{{ $user->sign_out_at }}</td>
                            @if(Auth::user()->isSuperOrAdmin())
                                <td class="center">
                                    <a href="{{ route('users.show', [ 'id' => $user->id ]) }}" class="btn btn-info btn-sm custom"><i class="glyphicon glyphicon-eye-open"></i> VIEW </a>
                                    @if ($user->role !== 'superadmin')
                                        <a href="{{ route('users.edit', [ 'id' => $user->id ]) }}" class="btn btn-warning btn-sm custom"><i class="glyphicon glyphicon-edit"></i> EDIT </a>
                                    @else
                                        <a href="{{ route('users.edit', [ 'id' => $user->id ]) }}" class="btn btn-warning btn-sm custom disabled"><i class="glyphicon glyphicon-edit"></i> EDIT </a>
                                    @endif
                                </td>
                                @if($user->role !== 'admin' && $user->role !== 'superadmin')
                                    <td class="center">
                                        <form action="{{ route('users.destroy', [ 'id' => $user->id ])}}" method="post" onsubmit="return confirm('Delete username {{ $user->name }}, email {{ $user->email }} permanently?')" >
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm" type="submit" ><i class="glyphicon glyphicon-trash"></i> DELETE</button>
                                        </form>
                                    </td>
                                @else
                                    <td class="center">
                                        <button class="btn btn-danger btn-sm disabled" type="submit" ><i class="glyphicon glyphicon-trash"></i> DELETE</button>
                                    </td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
    <!-- DataTables -->
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>

    <script>
        $(function () {
            $('#user-table').DataTable()
        })

    </script>
@endsection