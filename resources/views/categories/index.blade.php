@extends('layouts.master')

@section('top')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
    <h3 style="margin-top:0px">Categories</h3>
    @if(session('status'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{ session('status') }}
            </div>
        </div>
    @endif

    <div class="box-header">
        <a href="{{ route('categories.create') }}" class="btn btn-primary" ><i class="glyphicon glyphicon-plus"></i> Add Category</a>
    </div>

    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">List of Categories</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="category-table" class="table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        @if(Auth::user()->isSuperOrAdmin())
                            <th>Action</th>
                            <th>Delete</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->name }}</td>
                            @if(Auth::user()->isSuperOrAdmin())
                                <td class="center">
                                    <a href="{{ route('categories.show', [ 'id' => $category->id ]) }}" class="btn btn-info btn-sm custom"><i class="glyphicon glyphicon-eye-open"></i> VIEW</a>
                                    <a href="{{ route('categories.edit', [ 'id' => $category->id ]) }}" class="btn btn-warning btn-sm custom"><i class="glyphicon glyphicon-edit"></i> EDIT</a>
                                </td>
                                <td class="center">
                                    <form action="{{ route('categories.destroy', [ 'id' => $category->id ])}}" method="post" onsubmit="return confirm('Delete category {{ $category->name }} permanently?')" >
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" type="submit" ><i class="glyphicon glyphicon-trash"></i> DELETE</button>
                                    </form>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
    <!-- DataTables -->
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>

    <script>
        $(function () {
            $('#category-table').DataTable()
        })

    </script>
@endsection