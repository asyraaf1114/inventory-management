@extends('layouts.master')

@section('top')
@endsection

@section('content')
    <h3 style="margin-top:0px">Categories</h3>
    <div class="box-header">
        <a href="{{ route('categories.index') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> BACK</a>
    </div>
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">View Category</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped datatable">
                <tr>
                    <th>ID</th>
                    <td>{{ $category->id }}</td>
                </tr>

                <tr>
                    <th>Name</th>
                    <td>{{ $category->name }}</td>
                </tr>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
@endsection
