<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('user.png') }} " class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                {{-- <p>{{ \Auth::user()->name  }}</p> --}}
                <p>{{ \Auth::user()->email  }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!-- Optionally, you can add icons to the links -->
            <li class="{{  request()->is('home') ? 'active' : ''  }}"><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="header">USER SECTION</li>
            <li class="{{  request()->is('users') || request()->is('users/*') ? 'active' : ''  }}"><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> <span>User</span></a></li>
            <li class="{{  request()->is('profile') || request()->is('profile/*')  ? 'active' : ''  }}"><a href="{{ route('profile.index') }}"><i class="fa fa-user"></i> <span>My Profile</span></a></li>
            <li class="header">PRODUCT SECTION</li>
            <li class="{{  request()->is('categories') || request()->is('categories/*')  ? 'active' : ''  }}"><a href="{{ route('categories.index') }}"><i class="fa fa-files-o"></i> <span>Categories</span></a></li>
            <li class="{{  request()->is('products') || request()->is('products/*')  ? 'active' : ''  }}"><a href="{{ route('products.index') }}"><i class="fa fa-cube"></i> <span>Products</span></a></li>
            <li class="{{  request()->is('carts') || request()->is('carts/*')  ? 'active' : ''  }}"><a href="{{ route('carts.index') }}"><i class="fa fa-shopping-cart"></i> <span>Carts</span></a></li>
            <li class="{{  request()->is('sales') || request()->is('sales/*')  ? 'active' : ''  }}"><a href="{{ route('sales.index') }}"><i class="fa fa-file"></i> <span>Sales Report</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
