@extends('layouts.master')

@section('top')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
    <h3 style="margin-top:0px">Products</h3>
    @if(session('status'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{ session('status') }}
            </div>
        </div>
    @endif

    <div class="box-header">
        <a href="{{ route('products.create') }}" class="btn btn-primary" ><i class="glyphicon glyphicon-plus"></i> Add Product</a>
    </div>

    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">List of Products</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="product-table" class="table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Min</th>
                        <th>Category</th>
                        <th>Barcode</th>
                        <th>Image</th>
                        @if(Auth::user()->isSuperOrAdmin())
                            <th>Action</th>
                            <th>Delete</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>EM {{ $product->price }}</td>
                            <td>{{ $product->quantity }}</td>
                            <td>{{ $product->min_quantity }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td>{{ $product->barcode }}</td>
                            <td>
                                @if($product->image)
                                    <a href="{{ $product->image }}">
                                        <img src="{{ $product->image }}" width="50" >
                                    </a>
                                @endif
                            </td>
                            @if(Auth::user()->isSuperOrAdmin())
                                <td class="center">
                                    <a href="{{ route('products.show', [ 'id' => $product->id ]) }}" class="btn btn-info btn-sm custom"><i class="glyphicon glyphicon-eye-open"></i> VIEW</a>
                                    <a href="{{ route('products.edit', [ 'id' => $product->id ]) }}" class="btn btn-warning btn-sm custom"><i class="glyphicon glyphicon-edit"></i> EDIT</a>
                                </td>
                                <td class="center">
                                    <form action="{{ route('products.destroy', [ 'id' => $product->id ])}}" method="post" onsubmit="return confirm('Delete product {{ $product->name }} permanently?')" >
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" type="submit" ><i class="glyphicon glyphicon-trash"></i> DELETE</button>
                                    </form>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
    <!-- DataTables -->
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>

    <script>
        $(function () {
            $('#product-table').DataTable()
        })

    </script>
@endsection