@extends('layouts.master')

@section('top')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <h3 style="margin-top:0px">Products</h3>
    <!-- general form elements -->
    <div class="box-header">
        <a href="{{ route('products.index') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> BACK</a>
    </div>
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Update Product</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="POST" action="{{ route('products.update', ['id' => $product->id])  }}" enctype="multipart/form-data" autocomplete="off">
            @csrf
            @method('PATCH')

            <div class="box-body">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" value="{{ $product->name }}" id="name" class="form-control" placeholder="Enter name" required>
                    @if ($errors->has('name'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('name') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label >Price</label>
                    <input value="{{ $product->price }}" type="number" class="form-control" id="price" name="price" step="0.01" required>
                </div>

                <div class="form-group">
                    <label >Quantity</label>
                    <input value="{{ $product->quantity }}" type="number" class="form-control" id="quantity" name="quantity" required>
                </div>

                <div class="form-group">
                    <label >Minimum Quantity</label>
                    <input value="{{ $product->min_quantity }}" type="number" class="form-control" id="min_quantity" name="min_quantity" required>
                </div>

                <div class="form-group">
                    <label>Category</label>
                    <select class="form-control select_category" value="{{ $product->category }}" name="category_id" id="category_id" required>
                        {{-- <option value="">-- Select Category --</option> --}}
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Barcode</label>
                    <input type="text" name="barcode" value="{{ $product->barcode }}" id="barcode" class="form-control" placeholder="Enter barcode" required>
                    @if ($errors->has('barcode'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('barcode') }}
                        </div>
                    @endif
                </div>

                <div>
                    <label>Current Image</label>
                    <div>
                        <a href="{{ $product->image }}">
                            <img src="{{ $product->image }}" width="100" >
                        </a>
                    </div>
                </div>

                <div class="form-group">
                    <br>
                    <div class="alert alert-info" role="alert">
                        Leave the change image section below empty if you don't want to change.
                    </div>
                    </div>
                <div class="form-group">
                    <label >Change Image</label>
                    <input type="file" id="image" name="image" >
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> UPDATE</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
@endsection

@section('bot')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(".select_category").select2();
</script>
@endsection
