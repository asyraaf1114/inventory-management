@extends('layouts.master')

@section('top')
@endsection

@section('content')
    <h3 style="margin-top:0px">Products</h3>
    <div class="box-header">
        <a href="{{ route('products.index') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> BACK</a>
    </div>
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">View Product</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped datatable">
                <tr>
                    <th>ID</th>
                    <td>{{ $product->id }}</td>
                </tr>

                <tr>
                    <th>Name</th>
                    <td>{{ $product->name }}</td>
                </tr>

                <tr>
                    <th>Price</th>
                    <td>{{ $product->price }}</td>
                </tr>

                <tr>
                    <th>Quantity</th>
                    <td>{{ $product->quantity }}</td>
                </tr>

                <tr>
                    <th>Minimum Quantity</th>
                    <td>{{ $product->min_quantity }}</td>
                </tr>

                <tr>
                    <th>Category</th>
                    <td>{{ $product->category->name }}</td>
                </tr>

                <tr>
                    <th>Barcode</th>
                    <td>{{ $product->barcode }}</td>
                </tr>

                <tr>
                    <th>Image</th>
                    <td>
                        <a href="{{ $product->image }}">
                            <img src="{{ $product->image }}" width="100" >
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
@endsection
