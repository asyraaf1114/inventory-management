@extends('layouts.master')

@section('top')
@endsection

@section('content')
    <h3 style="margin-top:0px">Dashboard</h3>
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ \App\User::count() }}</h3>

                    <p>Users</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-people"></i>
                </div>
                <a href="{{ route('users.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Sample Progress Bar</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">                    
                    <b>Number of User</b>
                    <span class="pull-right">{{ \App\User::count() }}/10</span>
                    <div class="progress">
						<div class="progress-bar progress-bar-green " role="progressbar" aria-valuenow="{{ \App\User::count() }}" aria-valuemin="0" aria-valuemax="10" style="width: calc({{ \App\User::count() }}/10 *100%)"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('top')
@endsection


