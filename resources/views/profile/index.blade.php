@extends('layouts.master')

@section('top')
@endsection

@section('content')
    <h3 style="margin-top:0px">Profile</h3>

    @if(session('status'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{ session('status') }}
            </div>
        </div>
    @endif    

    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">My Profile Data</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped datatable">
                <tr>
                    <th>ID</th>
                    <td>{{ auth()->user()->id }}</td>
                </tr>

                <tr>
                    <th>Name</th>
                    <td>{{ auth()->user()->name }}</td>
                </tr>

                <tr>
                    <th>Email</th>
                    <td>{{ auth()->user()->email }}</td>
                </tr>

                <tr>
                    <th>Role</th>
                    <td>{{ auth()->user()->role }}</td>
                </tr>

                <tr>
                    <th>Previous Sign In</th>
                    <td>{{ auth()->user()->sign_in_at }}</td>
                </tr>

                <tr>
                    <th>Previous Sign Out</th>
                    <td>{{ auth()->user()->sign_out_at }}</td>
                </tr>

                <tr>
                    <th>Created at</th>
                    <td>{{ auth()->user()->created_at }}</td>
                </tr>

                <tr>
                    <th>Updated at</th>
                    <td>{{ auth()->user()->updated_at }}</td>
                </tr>
                <tr>
                    <th>Action</th>
                    <td>
                        <a href="{{ route('profile.edit', [ 'id' => auth()->user()->id ]) }}" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-edit"></i> EDIT</a>
                    </td>
                </tr>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('bot')
@endsection
