<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

date_default_timezone_set('Asia/Kuala_Lumpur');

Route::get('/', 'HomeController@checksession');

Auth::routes();

Route::group(['middleware' => 'App\Http\Middleware\Admin'], function() {
    Route::get('home', 'HomeController@index')->name('home');
    Route::resource('users', 'UserController');
    Route::resource('profile', 'ProfileController');
    Route::resource('categories', 'CategoryController');
    Route::resource('products', 'ProductController');
    Route::resource('carts', 'CartController');
    Route::get('/carts/add/{id}/{quantity}', 'CartController@add');
    Route::resource('sales', 'SaleController');
});

Route::group(['middleware' => 'App\Http\Middleware\User'], function() {
    Route::get('home', 'HomeController@index')->name('home');
    Route::resource('users', 'UserController', ['except' => 'edit', 'destroy']);
    Route::resource('profile', 'ProfileController', ['except' => 'edit', 'destroy']);
    Route::resource('categories', 'CategoryController',['except' => 'edit', 'destroy']);
    Route::resource('products', 'ProductController', ['except' => 'edit', 'destroy']);
    Route::resource('carts', 'CartController');
    Route::get('/carts/add/{id}/{quantity}', 'CartController@add');
    Route::resource('sales', 'SaleController', ['except' => 'edit', 'destroy']);
});

